## Initial Setup
1. Activati virtual env cu: source env/bin/activate
2. Intrati in subfolderul helloworld dupa ce ati activat venv si introduceti in terminal: python manage.py runserver
3. Dupa orice modificare in modele: - python manage.py makemigrations helloworld
									- python manage.py migrate